package me.dablakbandit.chatapi;

import me.dablakbandit.chatapi.packetlistener.PacketListener;
import me.dablakbandit.chatapi.packetlistener.players.PlayerManager;

import org.bukkit.plugin.java.JavaPlugin;

public class ChatAPIPlugin extends JavaPlugin{

	private static ChatAPIPlugin main;
	
	public static ChatAPIPlugin getInstance(){
		return main;
	}
	
	public void onLoad(){
		main = this;
		PacketListener.getInstance();
	}
	
	public void onEnable(){
		PacketListener.getInstance().setup();
		PlayerManager.getInstance();
	}
	
	public void onDisable(){
		PlayerManager.getInstance().unload();
	}
}
