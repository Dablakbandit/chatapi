package me.dablakbandit.chatapi;

import me.dablakbandit.chatapi.api.jsonformatter.JSONFormatter;
import me.dablakbandit.chatapi.api.jsonformatter.click.RunCommandEvent;
import me.dablakbandit.chatapi.api.jsonformatter.hover.ShowTextEvent;
import me.dablakbandit.chatapi.packetlistener.PacketListener;
import net.md_5.bungee.api.ChatColor;

import org.bukkit.entity.Player;

public class Example{

	// Firstly i recommend creating a method to send the JSONFormatter
	protected void send(JSONFormatter jf, Player player){
		// This allows the packets to go through
		PacketListener.getInstance().allow(player, jf.getSize());
		// This sends the packets
		jf.send(player);
	}

	// Another good method to have is one that stops chat packets from being
	// sent to the player
	protected void pause(Player player){
		PacketListener.getInstance().getListener().pause(player);
	}

	// And also one that restores the players chat after they have finished with
	// the gui
	protected void play(Player player){
		// This will resend the last 100 chat packets to player, if there is
		// only 50 to send,
		// then it will first send 50 blank packets
		PacketListener.getInstance().getListener().play(player);
	}

	// Basic send method
	public void send(Player player){
		// Whether newlines are enabled or not
		// Some minecraft version do not support new lines "\n"
		// It is best to have a setting in the config file to enable/disable this
		boolean newlines = false;
		JSONFormatter jf = new JSONFormatter(newlines);
		
		// Append a string
		// Some nice header
		jf.append("--------------");
		// This adds a new line
		jf.newLine();
		jf.append(" ");
		
		// Append a message with a click event
		// A click event is what happens when the player clicks it in their chat
		// There is RunCommandEvent or SuggestCommandEvent
		jf.appendClick(ChatColor.GREEN + "< Back", new RunCommandEvent("<command>" + " back"));
		jf.newLine();
		jf.newLine();
		jf.append(" " + ChatColor.RED + "Unsupported file");
		
		// There is also a hover event
		// There is ShowAchievementEvent, ShowItemEvent and ShowTextEvent
		jf.appendHover("Example", new ShowTextEvent("Hover"));
		
		// Resets just chat colors
		jf.resetColors();
		// Resets modifiers (italic, bold etc;)
		jf.resetModifiers();
		// Resets both, calling .newLine also resets all
		jf.resetAll();
		
		// Adds 15 blank lines
		// The Chat GUI can only be 20 lines high and you want it to be flush as to look nice
		jf.newLine(15);
		jf.append("--------------");
		
		// Calls the send method created earlier
		send(jf, player);
		
		// The JSONFormatter allows chaining which allows for very compact code
		jf.append("--------------").newLine()
		.append(" ").appendClick(ChatColor.GREEN + "< Back", new RunCommandEvent("<command>" + " back")).newLine().newLine()
		.append(" " + ChatColor.RED + "Unsupported file")
		.newLine(15)
		.append("--------------");
	}

}
