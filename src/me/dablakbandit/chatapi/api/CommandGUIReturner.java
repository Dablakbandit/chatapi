package me.dablakbandit.chatapi.api;

public abstract class CommandGUIReturner{

	public abstract void onReturn(String s);
	public abstract void onCancel();
}
