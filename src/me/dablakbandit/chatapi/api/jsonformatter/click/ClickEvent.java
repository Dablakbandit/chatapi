package me.dablakbandit.chatapi.api.jsonformatter.click;

import me.dablakbandit.chatapi.utils.json.JSONObject;

public abstract class ClickEvent {
	
	public abstract JSONObject getEvent();
	
}
