package me.dablakbandit.chatapi.api.jsonformatter.hover;

import me.dablakbandit.chatapi.utils.json.JSONObject;

public abstract class HoverEvent {
	
	public abstract JSONObject getEvent();
	
}
