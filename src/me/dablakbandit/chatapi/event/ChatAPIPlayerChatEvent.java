package me.dablakbandit.chatapi.event;

import me.dablakbandit.chatapi.packetlistener.players.Players;

import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.HandlerList;

public class ChatAPIPlayerChatEvent extends PlayersEvent implements Cancellable{

	private Object packet;
	private boolean cancelled;

	public ChatAPIPlayerChatEvent(Object packet, Players pl, Player player) {
		super(pl, player);
		this.packet = packet;
	}

	private static final HandlerList handlers = new HandlerList();

	@Override
	public HandlerList getHandlers(){
		return handlers;
	}
	
	public static HandlerList getHandlerList(){
		return handlers;
	}

	public Object getPacket(){
		return packet;
	}

	public void setPacket(Object o){
		this.packet = o;
	}

	@Override
	public boolean isCancelled(){
		return cancelled;
	}

	@Override
	public void setCancelled(boolean b){
		cancelled = b;
	}

}
