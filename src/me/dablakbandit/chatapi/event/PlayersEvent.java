package me.dablakbandit.chatapi.event;

import me.dablakbandit.chatapi.packetlistener.players.Players;

import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerEvent;

public abstract class PlayersEvent extends PlayerEvent{

	public Players pl;
	
	public PlayersEvent(Players pl, Player player){
		super(player);
		this.pl = pl;
	}
	
	public Players getPlayers(){
		return pl;
	}
}
