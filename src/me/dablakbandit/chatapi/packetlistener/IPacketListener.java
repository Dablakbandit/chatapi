package me.dablakbandit.chatapi.packetlistener;

import me.dablakbandit.chatapi.packetlistener.players.PlayerManager;
import me.dablakbandit.chatapi.packetlistener.players.Players;

import org.bukkit.Location;
import org.bukkit.entity.Player;

public abstract class IPacketListener {
	
	public abstract void setup();
	
	public abstract void setup(Player player);
	public abstract void remove(Player player);
	public abstract boolean cancel(Players pl, Object packet);
	
	public void pause(Player player){
		PlayerManager.getInstance().get(player).setPaused(true);
	}
	
	public void pause(String uuid){
		PlayerManager.getInstance().get(uuid).setPaused(true);
	}
	
	public void play(Player player){
		PlayerManager.getInstance().get(player).setPaused(false);
	}
	
	public void play(String uuid){
		PlayerManager.getInstance().get(uuid).setPaused(false);
	}
	
	public boolean isPaused(Player player){
		return PlayerManager.getInstance().get(player).getPaused();
	}
	
	public boolean isPaused(String uuid){
		return PlayerManager.getInstance().get(uuid).getPaused();
	}
	
	public abstract void sendCommandBlockOpen(Player player, Location loc, String def, String old);
	
}
