package me.dablakbandit.chatapi.packetlistener;

import me.dablakbandit.chatapi.api.CommandGUIReturner;
import me.dablakbandit.chatapi.packetlistener.implementation.chatapi.ChatAPIPacketListener;
import me.dablakbandit.chatapi.packetlistener.players.PlayerManager;
import me.dablakbandit.chatapi.packetlistener.players.Players;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;

public class PacketListener{

	private static PacketListener packetlistener = new PacketListener();

	public static PacketListener getInstance(){
		return packetlistener;
	}

	private IPacketListener listener;

	private PacketListener(){
		listener = new ChatAPIPacketListener();
	}

	public void setup(){
		listener.setup();
	}

	public void setup(Player player){
		listener.setup(player);
	}

	public void remove(Player player){
		listener.remove(player);
	}

	public IPacketListener getListener(){
		return listener;
	}

	public void setListener(IPacketListener listener){
		this.listener = listener;
	}

	public void allow(Player player, int amount){
		Players pl = PlayerManager.getInstance().get(player);
		if(pl==null)return;
		pl.setAllowed(amount);
	}

	@SuppressWarnings("deprecation")
	public void openCommandGUI(Player player, String def, String old, CommandGUIReturner ret){
		Players pl = PlayerManager.getInstance().get(player);
		Location l = player.getLocation();
		Location loc = new Location(l.getWorld(), l.getBlockX(), 0, l.getBlockZ());
		pl.setCommandReturner(loc, ret);
		player.sendBlockChange(loc, Material.COMMAND, (byte)0);
		listener.sendCommandBlockOpen(player, loc, def, old);
	}
}
