package me.dablakbandit.chatapi.packetlistener.implementation.chatapi;

import java.util.UUID;

import me.dablakbandit.chatapi.api.jsonformatter.JSONFormatter;
import me.dablakbandit.chatapi.packetlistener.IPacketListener;
import me.dablakbandit.chatapi.packetlistener.implementation.chatapi.channel.ChannelHandler;
import me.dablakbandit.chatapi.packetlistener.implementation.chatapi.channel.INCChannelHandler;
import me.dablakbandit.chatapi.packetlistener.implementation.chatapi.channel.NMUChannelHandler;
import me.dablakbandit.chatapi.packetlistener.players.PlayerManager;
import me.dablakbandit.chatapi.packetlistener.players.Players;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;

public class ChatAPIPacketListener extends IPacketListener{

	@Override
	public void setup() {
		initChannelHandler();
	}
	
	private ChannelHandler channelhandler;
	
	private boolean initChannelHandler(){
		try{
			Class.forName("net.minecraft.util.io.netty.channel.Channel");
			this.channelhandler = new NMUChannelHandler();
			System.out.print("[ChatAPI] Using NMUChannelHandler");
			return true;
		}catch(Exception e){}
		try{
			Class.forName("io.netty.channel.Channel");
			this.channelhandler = new INCChannelHandler();
			System.out.print("[ChatAPI] Using INCChannelHandler");
			return true;
		}catch(Exception e){}
		return false;
	}

	@Override
	public void setup(Player player) {
		channelhandler.addChannel(player);
	}

	@Override
	public void remove(Player player) {
		channelhandler.removeChannel(player);
	}

	@Override
	public boolean cancel(Players pl, Object packet) {
		if(pl.getPaused()){
			if(pl.getAllowed()>0){
				pl.setAllowed(pl.getAllowed()-1);
				return false;
			}
		}else if(pl.getAllowed()>0){
			pl.setAllowed(pl.getAllowed()-1);
			return false;
		}
		pl.getPackets().add(packet);
		while(pl.getPackets().size()>150){
			pl.getPackets().remove(0);
		}
		return pl.getPaused();
	}
	
	@Override
	public void play(Player player){
		super.play(player);
		Players pl = PlayerManager.getInstance().get(player);
		resend(pl, player);
	}
	
	@Override
	public void play(String uuid){
		super.play(uuid);
		Players pl = PlayerManager.getInstance().get(uuid);
		resend(pl, Bukkit.getPlayer(UUID.fromString(uuid)));
	}
	
	private static JSONFormatter jf = new JSONFormatter().append(" ");
	
	private void resend(Players pl, Player player){
		int i = 100 - pl.getAllowed();
		pl.setAllowed(i);
		while(i>0){
			i--;
			jf.send(player);
		}
		for(Object packet : pl.getPackets()){
			channelhandler.send(pl.getUUIDString(), packet, true);
		}
	}

	@Override
	public void sendCommandBlockOpen(Player player, Location loc, String def, String old){
		channelhandler.send(player, channelhandler.getCommandBlockPacket(loc, def, old), false);
	}
}
