package me.dablakbandit.chatapi.packetlistener.implementation.chatapi.channel;

import io.netty.channel.Channel;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

import me.dablakbandit.chatapi.event.ChatAPIPlayerChatEvent;
import me.dablakbandit.chatapi.packetlistener.PacketListener;
import me.dablakbandit.chatapi.packetlistener.players.Players;
import me.dablakbandit.chatapi.utils.ItemUtils;
import me.dablakbandit.chatapi.utils.NMSUtils;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;

public abstract class ChannelHandler{

	protected static Class<?> entityPlayer = getEntityPlayer();

	protected static Class<?> getEntityPlayer(){
		try{
			return NMSUtils.getNMSClassWithException("EntityPlayer");
		}catch(Exception e){}
		try{
			return Class.forName("net.minecraft.entity.player.EntityPlayerMP");
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	protected static Class<?> playerConnection = getPlayerConnection();

	protected static Class<?> getPlayerConnection(){
		try{
			return NMSUtils.getNMSClassWithException("PlayerConnection");
		}catch(Exception e){}
		try{
			return Class.forName("net.minecraft.network.NetHandlerPlayServer");
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	protected static Class<?> networkManager = getNetworkManager();

	protected static Class<?> getNetworkManager(){
		try{
			return NMSUtils.getNMSClassWithException("NetworkManager");
		}catch(Exception e){}
		try{
			return Class.forName("net.minecraft.network.NetworkManager");
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	protected static Class<?> packetPlayOutChat = getPacketPlayOutChat();

	protected static Class<?> getPacketPlayOutChat(){
		try{
			return NMSUtils.getNMSClassWithException("PacketPlayOutChat");
		}catch(Exception e){}
		try{
			return Class.forName("net.minecraft.network.play.server.S02PacketChat");
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	protected static Field channelField = getChannelField();

	protected static Field getChannelField(){
		try{
			return NMSUtils.getFirstFieldOfTypeWithException(networkManager, Channel.class);
		}catch(Exception e){
			System.out.print("Channel field not found");
		}
		return null;
	}
	

	protected static Field network = getNetworkField();

	protected static Field getNetworkField(){
		try{
			return NMSUtils.getFirstFieldOfTypeWithException(playerConnection, networkManager);
		}catch(Exception e){
			System.out.print("Network field not found");
		}
		return null;
	}

	protected static Field connection = getConnectionField();

	protected static Field getConnectionField(){
		try{
			return NMSUtils.getFirstFieldOfTypeWithException(entityPlayer, playerConnection);
		}catch(Exception e){
			System.out.print("Connection field not found");
		}
		return null;
	}

	protected static Field b = NMSUtils.getFirstFieldOfType(packetPlayOutChat, byte.class);

	protected static Class<?> packetPlayInCustomPayload = getPacketPlayInCustomPayload();

	protected static Class<?> getPacketPlayInCustomPayload(){
		try{
			return NMSUtils.getNMSClassWithException("PacketPlayInCustomPayload");
		}catch(Exception e){}
		try{
			return Class.forName("net.minecraft.network.play.client.C17PacketCustomPayload");
		}catch(Exception e){}
		try{
			return Class.forName("net.minecraft.network.play.client.CPacketCustomPayload");
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	protected static Field channel = NMSUtils.getFirstFieldOfType(packetPlayInCustomPayload, String.class);

	protected static Class<?> packetDataSerializer = getPacketDataSerializer();

	protected static Class<?> getPacketDataSerializer(){
		try{
			return NMSUtils.getNMSClassSilent("PacketDataSerializer");
		}catch(Exception e){}
		try{
			return Class.forName("net.minecraft.network.PacketBuffer");
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	protected static Field data = getData(packetPlayInCustomPayload);

	protected static Field getData(Class<?> c){
		try{
			return NMSUtils.getFirstFieldOfTypeWithException(c, byte[].class);
		}catch(Exception e){}
		try{
			return NMSUtils.getFirstFieldOfType(c, packetDataSerializer);
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	protected static Method c = getC();

	protected static Method getC(){
		try{
			NMSUtils.getMethod(packetDataSerializer, "c", int.class);
		}catch(Exception e){}
		try{
			NMSUtils.getMethod(packetDataSerializer, "func_150789_c", int.class);
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	protected static Method readableBytes = NMSUtils.getMethodSilent(packetDataSerializer, "readableBytes");

	protected static Method readByte = NMSUtils.getMethodSilent(packetDataSerializer, "readByte");
	protected static Method readInt = NMSUtils.getMethodSilent(packetDataSerializer, "readInt");

	protected static Class<?> packetPlayOutTileEntityData = getPacketPlayOutTileEntityData();

	protected static Class<?> getPacketPlayOutTileEntityData(){
		try{
			return NMSUtils.getNMSClassWithException("PacketPlayOutTileEntityData");
		}catch(Exception e){}
		try{
			return Class.forName("net.minecraft.network.play.server.SPacketUpdateTileEntity");
		}catch(Exception e){}
		try{
			return Class.forName("net.minecraft.network.play.server.S35PacketUpdateTileEntity");
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	protected static Constructor<?> ppotedc = NMSUtils.getConstructor(packetPlayOutTileEntityData);

	protected static Class<?> blockPosition = getBlockPosition();

	protected static Class<?> getBlockPosition(){
		try{
			return NMSUtils.getNMSClassWithException("BlockPosition");
		}catch(Exception e){}
		try{
			return Class.forName("net.minecraft.util.math.BlockPos");
		}catch(Exception e){}
		return null;
	}

	protected static Constructor<?> bpc = NMSUtils.getConstructorSilent(blockPosition, int.class, int.class, int.class);

	protected static Field eda = NMSUtils.getFieldSilent(packetPlayOutTileEntityData, "a");
	protected static Field edb = NMSUtils.getFieldSilent(packetPlayOutTileEntityData, "b");
	protected static Field edc = NMSUtils.getFieldSilent(packetPlayOutTileEntityData, "c");
	protected static Field edd = NMSUtils.getFieldSilent(packetPlayOutTileEntityData, "d");
	protected static Field ede = NMSUtils.getFieldSilent(packetPlayOutTileEntityData, "e");

	protected static Class<?> packetPlayInLook = getPacketPlayInLook();

	protected static Class<?> getPacketPlayInLook(){
		try{
			return NMSUtils.getNMSClass("PacketPlayInLook", "PacketPlayInFlying");
		}catch(Exception e){}
		try{
			return Class.forName("net.minecraft.network.play.client.C03PacketPlayer$C05PacketPlayerLook");
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	public boolean write(Object msg, Players pl, Player player){
		if(msg.getClass().equals(packetPlayOutChat)){
			try{
				byte a = b != null ? (byte)b.get(msg) : 0;
				if(a == 0 || a == 1){
					ChatAPIPlayerChatEvent event = new ChatAPIPlayerChatEvent(msg, pl, player);
					Bukkit.getPluginManager().callEvent(event);
					if(event.isCancelled()){ return false; }
					msg = event.getPacket();
					if(PacketListener.getInstance().getListener().cancel(pl, msg)){ return false; }
				}
			}catch(Exception e){
				e.printStackTrace();
			}
		}else if(msg.getClass().equals(packetPlayOutTileEntityData)){
			/*try{
				if(blockPosition == null){

				}else{
					Object tag = edc.get(msg);
					JSONObject jo = new JSONObject();
					JSONObject helper = new JSONObject();

					ItemUtils.convertCompoundTagToJSON(tag, jo, helper);
					System.out.print(jo.toString());
					System.out.print(helper.toString());
				}
			}catch(Exception e){
				e.printStackTrace();
			}*/
		}
		return true;
	}

	public boolean read(Object msg, Players pl, Player player){
		/*if(msg.getClass().equals(packetPlayInCustomPayload)){
			if(pl.getCommandReturner() != null){
				try{
					String tag = (String)channel.get(msg);
					System.out.print(tag);
					String set = null;
					if(tag.equals("MC|AdvCdm") || tag.equals("MC|AdvCmd")){
						if(data.getType().equals(byte[].class)){
							byte[] b = (byte[])data.get(msg);
							ByteArrayInputStream bis = new ByteArrayInputStream(b);
							DataInputStream dis = new DataInputStream(bis);
							byte action = dis.readByte();
							if(action == 0){
								dis.readInt();
								dis.readInt();
								dis.readInt();
							}else{
								dis.readInt();
							}
							byte[] bytes = new byte[dis.available()];
							dis.read(bytes);
							set = new String(bytes, Charsets.UTF_8).trim();
						}else{
							Object o = data.get(msg);
							byte action = (byte)readByte.invoke(o);
							if(action == 0){
								readInt.invoke(o);
								readInt.invoke(o);
								readInt.invoke(o);
							}else if(action == 1){
								readInt.invoke(o);
							}
							set = (String)c.invoke(o, readableBytes.invoke(o));
						}
					}else if(tag.equals("MC|AutoCmd")){
						Object o = data.get(msg);
						readInt.invoke(o);
						readInt.invoke(o);
						readInt.invoke(o);
						set = (String)c.invoke(o, readableBytes.invoke(o));
					}
					if(set != null){
						System.out.print(set);
						pl.getCommandReturner().onReturn(set);
						pl.removeCommandGUI(player);
						return false;
					}
				}catch(Exception e){
					e.printStackTrace();
				}
			}
		}else if(msg.getClass().equals(packetPlayInLook)){
			if(pl.getCommandReturner()!=null)pl.removeCommandGUI(player);
		}*/
		return true;
	}

	public Object getCommandBlockPacket(Location loc, String def, String old){
		try{
			Object packet = ppotedc.newInstance();
			Object tag = ItemUtils.getNewNBTTagCompound();
			ItemUtils.set(tag, "conditionMet", ItemUtils.getNewNBTTagByte((byte)0));
			ItemUtils.set(tag, "auto", ItemUtils.getNewNBTTagByte((byte)0));
			ItemUtils.setString(tag, "CustomName", "@");
			ItemUtils.set(tag, "powered", ItemUtils.getNewNBTTagByte((byte)0));
			ItemUtils.setString(tag, "Command", def != null ? def : "");
			ItemUtils.setInt(tag, "x", loc.getBlockX());
			ItemUtils.setInt(tag, "y", loc.getBlockY());
			ItemUtils.setInt(tag, "z", loc.getBlockZ());
			ItemUtils.setString(tag, "id", "Control");
			ItemUtils.setInt(tag, "SuccessCount", 0);
			//if(old!=null)ItemUtils.setString(tag, "LastOutput", "" + new JSONFormatter().append(old).toJSON().toString());
			ItemUtils.set(tag, "trackOutput", ItemUtils.getNewNBTTagByte((byte)1));
			if(blockPosition==null){
				eda.set(packet, loc.getBlockX());
				edb.set(packet, loc.getBlockY());
				edc.set(packet, loc.getBlockZ());
				edd.set(packet, 2);
				edc.set(packet, tag);
			}else{
				eda.set(packet, bpc.newInstance(loc.getBlockX(), loc.getBlockY(), loc.getBlockZ()));
				edb.set(packet, 2);
				edc.set(packet, tag);
			}
			return packet;
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	public abstract void addChannel(Player player);

	public abstract void removeChannel(Player player);

	public abstract void disable();

	public abstract void send(Player player, Object packet, boolean bypass);

	public abstract void send(String uuid, Object packet, boolean bypass);

}