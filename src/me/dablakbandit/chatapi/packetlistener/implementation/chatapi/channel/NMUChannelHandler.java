package me.dablakbandit.chatapi.packetlistener.implementation.chatapi.channel;

import java.util.HashMap;
import java.util.Map;

import me.dablakbandit.chatapi.packetlistener.players.PlayerManager;
import me.dablakbandit.chatapi.packetlistener.players.Players;
import me.dablakbandit.chatapi.utils.NMSUtils;
import net.minecraft.util.io.netty.channel.Channel;
import net.minecraft.util.io.netty.channel.ChannelDuplexHandler;
import net.minecraft.util.io.netty.channel.ChannelHandlerContext;
import net.minecraft.util.io.netty.channel.ChannelPromise;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerKickEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class NMUChannelHandler extends ChannelHandler implements Listener{

	public NMUChannelHandler(){}

	public void disable(){
		for(Player player : Bukkit.getOnlinePlayers()){
			removeChannel(player);
		}
	}

	@EventHandler
	public void onPlayerJoin(PlayerJoinEvent event){
		addChannel(event.getPlayer());
	}

	@EventHandler
	public void onPlayerQuit(PlayerQuitEvent event){
		removeChannel(event.getPlayer());
	}

	@EventHandler
	public void onPlayerKick(PlayerKickEvent event){
		removeChannel(event.getPlayer());
	}

	@Override
	public void addChannel(final Player player){
		removeChannel(player);
		try{
			final Object handle = NMSUtils.getHandle(player);
			final Object connection = NMUChannelHandler.connection.get(handle);
			final Channel channel = (Channel)channelField.get(network.get(connection));
			new Thread(new Runnable(){
				@Override
				public void run(){
					try{
						channel.pipeline().addBefore("packet_handler", "chatapi_listener_player", new PlayerChannelHandler(player));
					}catch(Exception e){}
				}
			}, "ChatAPI Player Channel Adder").start();
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	@Override
	public void removeChannel(final Player player){
		try{
			final Object handle = NMSUtils.getHandle(player);
			final Object connection = NMUChannelHandler.connection.get(handle);
			final Channel channel = (Channel)channelField.get(network.get(connection));
			new Thread(new Runnable(){
				@Override
				public void run(){
					try{
						channel.pipeline().remove("chatapi_listener_player");
						handlers.remove(player.getUniqueId().toString());
					}catch(Exception e){}
				}
			}, "ChatAPI Player Channel Remover").start();
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	private static Map<String, PlayerChannelHandler> handlers = new HashMap<String, PlayerChannelHandler>();

	private class PlayerChannelHandler extends ChannelDuplexHandler{

		private Player player;
		private Players pl;

		public PlayerChannelHandler(Player player){
			this.player = player;
			this.pl = PlayerManager.getInstance().get(player);
			handlers.put(player.getUniqueId().toString(), this);
		}

		private ChannelHandlerContext chc;

		@Override
		public void write(ChannelHandlerContext ctx, Object msg, ChannelPromise promise) throws Exception{
			if(chc == null)
				chc = ctx;
			if(NMUChannelHandler.this.write(msg, pl, player))super.write(ctx, msg, promise);
		}

		@Override
		public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception{
			if(chc == null)
				chc = ctx;
			if(NMUChannelHandler.this.read(msg, pl, player))super.channelRead(ctx, msg);
		}

		public void bypass(Object packet, boolean bypass) throws Exception{
			if(bypass)super.write(chc, packet, chc.newPromise());
			else write(chc, packet, chc.newPromise());
		}
	}

	@Override
	public void send(Player player, Object packet, boolean bypass){
		try{
			handlers.get(player.getUniqueId().toString()).bypass(packet, bypass);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	@Override
	public void send(String uuid, Object packet, boolean bypass){
		try{
			handlers.get(uuid).bypass(packet, bypass);
		}catch(Exception e){
			e.printStackTrace();
		}
	}
}