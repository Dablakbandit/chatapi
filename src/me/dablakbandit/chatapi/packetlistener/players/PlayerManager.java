package me.dablakbandit.chatapi.packetlistener.players;

import java.util.HashMap;
import java.util.Map;

import me.dablakbandit.chatapi.ChatAPIPlugin;
import me.dablakbandit.chatapi.packetlistener.PacketListener;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerKickEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class PlayerManager implements Listener{

	private static PlayerManager manager = new PlayerManager();
	
	public static PlayerManager getInstance(){
		return manager;
	}
	
	private PlayerManager(){
		Bukkit.getPluginManager().registerEvents(this, ChatAPIPlugin.getInstance());
		for(Player player : Bukkit.getOnlinePlayers()){
			add(player);
		}
	}
	
	public void unload(){
		for(Player player : Bukkit.getOnlinePlayers()){
			remove(player);
		}
	}
	
	private Map<String, Players> players = new HashMap<String, Players>();
	
	private void add(Player player){
		Players pl = get(player);
		if(pl!=null)return;
		pl = new Players(player);
		pl.load();
		players.put(pl.getUUIDString(), pl);
		PacketListener.getInstance().setup(player);
	}
	
	public Players get(Player player){
		return players.get(player.getUniqueId().toString());
	}
	
	public Players get(String uuid){
		return players.get(uuid);
	}
	
	private void remove(Player player){
		Players pl = get(player);
		if(pl==null)return;
		pl.unload();
		players.remove(pl.getUUIDString());
		PacketListener.getInstance().remove(player);
	}
	
	@EventHandler
	public void onPlayerJoin(PlayerJoinEvent event){
		add(event.getPlayer());
	}
	
	@EventHandler
	public void onPlayerKick(PlayerKickEvent event){
		remove(event.getPlayer());
	}
	
	@EventHandler
	public void onPlayerQuit(PlayerQuitEvent event){
		remove(event.getPlayer());
	}
	
}
