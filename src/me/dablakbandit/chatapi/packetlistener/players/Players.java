package me.dablakbandit.chatapi.packetlistener.players;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import me.dablakbandit.chatapi.api.CommandGUIReturner;

import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;

public class Players {

	private String uuid;
	private List<Object> packets = new ArrayList<Object>();
	private boolean paused = false;
	private int allowed = 0;
	private CommandGUIReturner returner;
	
	public Players(Player player){
		this.uuid = player.getUniqueId().toString();
	}
	
	public String getUUIDString(){
		return uuid;
	}
	
	public UUID getUUID(){
		return UUID.fromString(uuid);
	}
	
	public void load(){}
	
	public void unload(){}
	
	public List<Object> getPackets(){
		return packets;
	}
	
	public boolean getPaused(){
		return paused;
	}
	
	public void setPaused(boolean b){
		paused = b;
	}
	
	public int getAllowed(){
		return allowed;
	}
	
	public void setAllowed(int i){
		allowed = i;
	}
	
	public CommandGUIReturner getCommandReturner(){
		return returner;
	}
	
	private Location fake;
	
	public void setCommandReturner(Location fake, CommandGUIReturner returner){
		this.returner = returner;
		this.fake = fake;
	}
	
	@SuppressWarnings("deprecation")
	public void removeCommandGUI(Player player){
		Block b = fake.getBlock();
		player.sendBlockChange(fake, b.getType(), b.getData());
		returner.onCancel();
		this.returner = null;
		fake = null;
	}
	
}
