package me.dablakbandit.chatapi.utils.json;

public interface JSONString {

    public String toJSONString();
}
